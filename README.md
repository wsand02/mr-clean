# Mr. Clean

A discord bot made with the sole intent of cleaning up the mess your friends made on your discord server.

## Installation
Add the discord bot token to an environment variable called `MR_CLEAN_TOKEN`.

Run `pipenv install`.

Then if you are on windows run `pipenv run py bot.py`

or on linux `pipenv run python bot.py`.
