import os

import discord
from discord.ext import commands

bot = commands.Bot(command_prefix='<')


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('-' * len(str(bot.user.id)))


@bot.command()
async def ping(ctx):
    await ctx.send('pong')


@bot.command()
@commands.has_permissions(manage_messages=True)
async def clean(ctx, amount: int):
    channel = ctx.message.channel
    deleted = await channel.purge(limit=amount)
    if (amount > 100):
        amount = 100
    await ctx.send(f'Mr. Clean did it again!😉', delete_after=10)


@clean.error
async def clean_error(ctx, error):
    if isinstance(error, commands.MissingPermissions):
        await ctx.send(f"I saw what you where doing there {ctx.author.name}!")


bot.run(os.environ.get('MR_CLEAN_TOKEN'))
